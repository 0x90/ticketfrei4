from tortoise import fields
from tortoise.contrib.pydantic import pydantic_model_creator, pydantic_queryset_creator
from tortoise.models import Model


class Fahrt(Model):
    id = fields.IntField(pk=True)
    linie = fields.TextField()
    verlauf: fields.ReverseRelation["Halt"]


class Halt(Model):
    id = fields.IntField(pk=True)
    fahrt = fields.ForeignKeyField("models.Fahrt", "verlauf")
    haltestelle = fields.ForeignKeyField("models.Haltestelle", "fahrten")
    ankunft = fields.DatetimeField(null=True)
    abfahrt = fields.DatetimeField(null=True)


class Haltestelle(Model):
    id = fields.IntField(pk=True)
    name = fields.TextField()
    longitude = fields.FloatField(null=True)
    latitude = fields.FloatField(null=True)
    fahrten: fields.ReverseRelation["Halt"]


Fahrt_Pydantic = pydantic_model_creator(Fahrt)
Halt_Pydantic = pydantic_model_creator(Halt)
Haltestelle_Pydantic = pydantic_model_creator(Haltestelle)

Fahrt_Pydantic_List = pydantic_queryset_creator(Fahrt)
Halt_Pydantic_List = pydantic_queryset_creator(Halt)
Haltestelle_Pydantic_List = pydantic_queryset_creator(Haltestelle)
