import uvicorn


def main() -> None:
    uvicorn.run("ticketfrei3.application:app")
